package com.yys.deal.base;

/**
 * Created by Yanyunsong on 2017/5/3.
 */
public abstract class AbstractDeal {

    public abstract void work();
}
