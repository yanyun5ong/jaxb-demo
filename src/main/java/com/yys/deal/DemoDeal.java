package com.yys.deal;

import com.yys.annotation.*;
import com.yys.bean.User;
import com.yys.bean.base.MsgHead;
import com.yys.deal.base.AbstractDeal;
import com.yys.dto.UserRequestDto;
import com.yys.dto.UserResponseDto;

/**
 * 测试交易,交易码TEST001
 */
@MsgCode(code = "TEST001")
public class DemoDeal extends AbstractDeal {

    @RequestField
    private UserRequestDto data;

    @ResponseField
    private UserResponseDto resp;

    @Override
    public void work() {
        System.out.println("DemoDeal.requestObj= " + this.data);


        /* 交易处理开始 */
        MsgHead requestHead = data.getHead();
        MsgHead respHead  = new MsgHead();
        respHead.setMsgCode(requestHead.getMsgCode());
        respHead.setDestination(requestHead.getSource());
        respHead.setSource(requestHead.getDestination());

        User user = data.getBody();
        user.setUserNickName("Jack");

        resp = new UserResponseDto();
        resp.setHead(respHead);
        resp.setBody(user);
        /* 交易处理结束 */

    }

    public UserRequestDto getData() {
        return data;
    }

    public void setData(UserRequestDto data) {
        this.data = data;
    }

    public UserResponseDto getResp() {
        return resp;
    }

    public void setResp(UserResponseDto resp) {
        this.resp = resp;
    }
}
