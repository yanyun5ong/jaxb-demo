package com.yys.util;

import org.apache.commons.lang.ArrayUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * 封装了XML转换成object，object转换成XML的代码
 *
 * @author Steven
 */
public class JaxbUtil {
    /**
     * 将对象直接转换成String类型的 XML输出
     *
     * @param obj
     * @return
     * @throws JAXBException
     */
    public static String marshal(Object obj, Class<?>... clazz) throws JAXBException {
        // 创建输出流
        StringWriter sw = new StringWriter();

        // 利用jdk中自带的转换类实现
        JAXBContext context = JAXBContext.newInstance((Class[]) ArrayUtils.add(clazz,obj.getClass()));

        Marshaller marshaller = context.createMarshaller();
        // 格式化xml输出的格式
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
        // 将对象转换成输出流形式的xml
        marshaller.marshal(obj, sw);
        return sw.toString();
    }

    /**
     * 将对象根据路径转换成xml文件
     *
     * @param obj
     * @param path
     * @return
     * @throws JAXBException
     */
    public static void marshalToFile(Object obj, String path, Class<?>... clazz) throws JAXBException, IOException {
        // 利用jdk中自带的转换类实现
        JAXBContext context = JAXBContext.newInstance((Class[]) ArrayUtils.add(clazz,obj.getClass()));

        Marshaller marshaller = context.createMarshaller();
        // 格式化xml输出的格式
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        // 将对象转换成输出流形式的xml
        // 创建输出流
        FileWriter fw = new FileWriter(path);
        marshaller.marshal(obj, fw);
    }

    /**
     * 将String类型的xml转换成对象
     *
     * @throws JAXBException
     */
    public static Object unmarshal(String xmlStr, Class<?>... clazz) throws JAXBException {
        Object xmlObject = null;
        JAXBContext context = JAXBContext.newInstance(clazz);
        // 进行将Xml转成对象的核心接口
        Unmarshaller unmarshaller = context.createUnmarshaller();
        StringReader sr = new StringReader(xmlStr);
        xmlObject = unmarshaller.unmarshal(sr);
        return xmlObject;
    }

    /**
     * 将file类型的xml转换成对象
     *
     * @throws JAXBException
     */
    public static Object unmarshalFromFile(Class<?> clazz, String xmlPath) throws JAXBException, FileNotFoundException {
        Object xmlObject = null;
        JAXBContext context = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        FileReader fr = new FileReader(xmlPath);
        xmlObject = unmarshaller.unmarshal(fr);
        return xmlObject;
    }
}
