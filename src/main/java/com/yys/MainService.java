package com.yys;

import com.yys.base.DealHandler;
import com.yys.deal.base.AbstractDeal;
import org.apache.commons.io.FileUtils;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;



public class MainService {

    public static void main(String[] args) {
        try {
            String path = ClassLoader.getSystemResource("input.xml").getPath();
            File inputFile = new File(path);
            String messageContent = FileUtils.readFileToString(inputFile, "UTF-8");
            AbstractDeal deal = DealHandler.unmarshal(messageContent);//解包
            deal.work();//交易处理
            String xml = DealHandler.marshal(deal);//组包
            System.out.println("output xml: " + xml);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

}
