package com.yys.bean.base;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class AbstractResponse<T> {

    protected MsgHead head;

    protected T body;

    public abstract MsgHead getHead();

    public abstract void setHead(MsgHead value);

    public abstract T getBody();

    public abstract void setBody(T value);

    @Override
    public String toString() {
        return "AbstractResponse{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
