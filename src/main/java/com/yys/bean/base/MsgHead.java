package com.yys.bean.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "version", "source","destination","msgCode","transNoSource","transNoDestination" })
public class MsgHead {

    private String version;

    private String source;

    private String destination;

    private String msgCode;

    private String transNoSource;

    private String transNoDestination;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTransNoSource() {
        return transNoSource;
    }

    public void setTransNoSource(String transNoSource) {
        this.transNoSource = transNoSource;
    }

    public String getTransNoDestination() {
        return transNoDestination;
    }

    public void setTransNoDestination(String transNoDestination) {
        this.transNoDestination = transNoDestination;
    }

    @Override
    public String toString() {
        return "MsgHead{" +
                "version='" + version + '\'' +
                ", source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", msgCode='" + msgCode + '\'' +
                ", transNoSource='" + transNoSource + '\'' +
                ", transNoDestination='" + transNoDestination + '\'' +
                '}';
    }
}
