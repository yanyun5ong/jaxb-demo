package com.yys.base;

import com.yys.annotation.MsgCode;
import com.yys.annotation.RequestField;
import com.yys.annotation.ResponseField;
import com.yys.deal.DemoDeal;
import com.yys.deal.base.AbstractDeal;
import com.yys.util.JaxbUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yanyunsong on 2017/5/3.
 */
public class DealHandler {

    private static final Map<String, Class> classMap = new HashMap<String, Class>();


    private static void loadDealClass() {
        classMap.clear();

        String path = AbstractDeal.class.getResource("../").getFile();
        File parent = new File(path);
        File[] children = parent.listFiles();
        for (File file : children) {
            if (file.isFile()) {
                String classname = file.getName();
                try {
                    Class<?> clazz = Class.forName("com.yys.deal." + classname.replace(".class", ""));
                    MsgCode msgCode = clazz.getAnnotation(MsgCode.class);
                    classMap.put(msgCode.code(), clazz);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    /**
     * 通过XML字符串找到交易类,并将BODY中的信息封装到交易类的数据对象中
     *
     * @param messageContent XML字符串
     * @return
     * @throws JAXBException
     * @throws ReflectiveOperationException
     */
    public static AbstractDeal unmarshal(String messageContent) throws JAXBException, ReflectiveOperationException {
        int start = StringUtils.indexOf(messageContent, "<msgCode>");
        int end = StringUtils.indexOf(messageContent, "</msgCode>");
        String msgCode = StringUtils.replace(StringUtils.substring(messageContent, start, end), "<msgCode>", "");

        loadDealClass();
        Class clazz = classMap.get(msgCode);
        if(null==clazz){
            return null;
        }

        //TODO: 根据交易码找到处理类,此处为了方便测试,使用固定的类
        AbstractDeal deal = (AbstractDeal)clazz.newInstance();

        Class typeClazz = deal.getClass();
        Field[] fields = typeClazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(RequestField.class)) {
                Class<?> fieldClazz = field.getType();
                Object obj = JaxbUtil.unmarshal(messageContent, fieldClazz);

                String capitalizeFileldName = StringUtils.capitalize(field.getName());
                Method setData = typeClazz.getMethod("set" + capitalizeFileldName, fieldClazz);//调用set方法设置值
                setData.invoke(deal, obj);
                break;
            }
        }
        return deal;
    }

    public static String marshal(AbstractDeal deal) throws JAXBException, ReflectiveOperationException {
        Class typeClazz = deal.getClass();
        Field[] fields = typeClazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(ResponseField.class)) {
                Class<?> fieldClazz = field.getType();

                String capitalizeFileldName = StringUtils.capitalize(field.getName());
                Method setData = typeClazz.getMethod("get" + capitalizeFileldName);
                Object obj = setData.invoke(deal);

                return JaxbUtil.marshal(obj, fieldClazz);
            }
        }
        return null;
    }

}
