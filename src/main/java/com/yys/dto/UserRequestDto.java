package com.yys.dto;

import com.yys.bean.User;
import com.yys.bean.base.AbstractRequest;
import com.yys.bean.base.MsgHead;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Enveloper")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"head", "body"})
public class UserRequestDto extends AbstractRequest<User> {

    @XmlElement(name = "HEAD", required = true)
    @Override
    public MsgHead getHead() {
        return head;
    }

    @Override
    public void setHead(MsgHead value) {
        this.head = value;
    }

    @XmlElement(name = "BODY", required = true)
    @Override
    public User getBody() {
        return body;
    }

    @Override
    public void setBody(User value) {
        this.body=value;
    }

}
