package com.yys.dto;

import com.yys.bean.User;
import com.yys.bean.base.AbstractResponse;
import com.yys.bean.base.MsgHead;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Enveloper")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"head", "body"})
public class UserResponseDto extends AbstractResponse<User> {

    @XmlElement(name = "HEAD", required = true)
    @Override
    public MsgHead getHead() {
        return head;
    }

    @Override
    public void setHead(MsgHead value) {
        head = value;
    }

    @XmlElement(name = "BODY", required = true)
    @Override
    public User getBody() {
        return body;
    }

    @Override
    public void setBody(User value) {
        body = value;
    }
}
